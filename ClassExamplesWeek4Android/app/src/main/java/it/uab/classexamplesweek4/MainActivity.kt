package it.uab.classexamplesweek4

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.room.Room
import it.uab.classexamplesweek4.databinding.ActivityMainBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity(), Callback<CivilizationContainer> {
    private lateinit var binding: ActivityMainBinding
    private lateinit var prefs: SharedPreferences
    private lateinit var civDao: CivilizationDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val db = Room.databaseBuilder(
            applicationContext,
            CivilizationDatabase::class.java, CivilizationDatabase.databaseName
        ).build()
        civDao = db.civDao()

        prefs = getSharedPreferences("week4civs", MODE_PRIVATE)
        binding.civRecycler.adapter = CivAdapter(civDao)

        //clearStorage()
        loadData()
    }

    private fun loadData() {
        if (prefs.getBoolean("isDownloaded", false)) {
            runOnIO {
                (binding.civRecycler.adapter as CivAdapter).update(civDao.getAll())
            }
        } else {
            CivWebservice().civilizationService.getCivs().enqueue(this)
        }
    }

    private fun clearStorage() {
        runOnIO {
            prefs.edit().putBoolean("isDownloaded", false).apply()
            civDao.deleteAll()
        }
    }

    override fun onResponse(
        call: Call<CivilizationContainer>,
        response: Response<CivilizationContainer>
    ) {
        if (response.isSuccessful) {
            Log.d("onResponse", "download success!")
            val civs = response.body()?.civilizations
            civs?.let {
                (binding.civRecycler.adapter as CivAdapter).update(civs)
                runOnIO {
                    civDao.insertAll(civs)
                }
            }
            prefs.edit().putBoolean("isDownloaded", true).apply()
        }
    }

    override fun onFailure(call: Call<CivilizationContainer>, t: Throwable) {
        Log.e("onFailure", t.message!!)
    }
}

fun runOnIO(lambda: suspend () -> Unit) {
    runBlocking {
        launch(Dispatchers.IO) { lambda() }
    }
}