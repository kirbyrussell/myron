package it.uab.classexamplesweek4

import android.app.AlertDialog
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.EditText
import androidx.recyclerview.widget.RecyclerView
import it.uab.classexamplesweek4.databinding.ItemCivBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class CivAdapter(private val civDao: CivilizationDao): RecyclerView.Adapter<CivAdapter.ViewHolder>() {
    private val data = mutableListOf<Civilization>()

    class ViewHolder(val binding: ItemCivBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemCivBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val civ = data[position]
        holder.binding.civName.text = "${civ.name} from ${civ.expansion}"
        holder.binding.civBonus.text = civ.bonus
        holder.itemView.setOnClickListener {
            val editText = EditText(holder.itemView.context)
            editText.setText(civ.name)

            val builder = AlertDialog.Builder(holder.itemView.context)
                .setTitle(civ.name)
                .setView(editText)
                .setPositiveButton("Save") {_,_ ->
                    civ.name = editText.text.toString()
                    runOnIO { civDao.update(civ) }
                    notifyItemChanged(position)
                }
                .setNeutralButton("Cancel") {_,_ -> }
                .setNegativeButton("Delete") {_,_ ->
                    runOnIO { civDao.delete(civ) }
                    data.removeAt(position)
                    notifyItemRemoved(position)
                }
                .create().show()
        }
    }

    override fun getItemCount(): Int = data.size

    fun update(list: List<Civilization>?) {
        list?.let {
            data.addAll(it)
            notifyDataSetChanged()
        }
    }
}