package it.uab.classexamplesweek4

import androidx.room.*
import com.google.gson.annotations.SerializedName

data class CivilizationContainer(
    val civilizations: List<Civilization>
)

@Entity
data class Civilization (
    @PrimaryKey val id: Long,
    var name: String,
    val expansion: String,
    @SerializedName("team_bonus") val bonus: String,
    val setId: Long
)

@Dao
interface CivilizationDao {
    @Query("select * from civilization")
    suspend fun getAll(): List<Civilization>

    @Insert
    suspend fun insert(civ: Civilization)

    @Insert
    suspend fun insertAll(civ: List<Civilization>)

    @Update
    suspend fun update(civ: Civilization)

    @Delete
    suspend fun delete(civ: Civilization)

    @Query("delete from civilization")
    suspend fun deleteAll()
}

/*
{
      "id": 1,
      "name": "Aztecs",
      "expansion": "The Conquerors",
      "army_type": "Infantry and Monk",
      "unique_unit": [
        "https://age-of-empires-2-api.herokuapp.com/api/v1/unit/jaguar_warrior"
      ],
      "unique_tech": [
        "https://age-of-empires-2-api.herokuapp.com/api/v1/technology/garland_wars"
      ],
      "team_bonus": "Relics generate +33% gold",
      "civilization_bonus": [
        "Villagers carry +5",
        "Military units created 15% faster",
        "+5 Monk hit points for each Monastery technology",
        "Loom free"
      ]
    },
 */