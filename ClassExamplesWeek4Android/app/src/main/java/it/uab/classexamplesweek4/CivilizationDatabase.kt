package it.uab.classexamplesweek4

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [Civilization::class], version = 1)
abstract class CivilizationDatabase: RoomDatabase() {
    companion object {
        const val databaseName = "CIVILIZATION_DATABASE"
    }

    abstract fun civDao() : CivilizationDao
}