package it.uab.classexamplesweek4

import com.google.gson.annotations.SerializedName
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

class CivWebservice {
    val retrofit = Retrofit.Builder()
        .baseUrl("https://age-of-empires-2-api.herokuapp.com/api/v1/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()
    val civilizationService = retrofit.create(CivilizationService::class.java)

    interface CivilizationService {
        @GET("civilizations")
        fun getCivs(): Call<CivilizationContainer>
    }
}