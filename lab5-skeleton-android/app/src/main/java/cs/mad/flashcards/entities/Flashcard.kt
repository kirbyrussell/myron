package cs.mad.flashcards.entities

import androidx.room.*

@Entity
data class Flashcard(

     var question: String,
    var answer: String, @PrimaryKey val id: Long? = null ,
    val setId: Long
) {

    companion object {
        fun getHardcodedFlashcards(): List<Flashcard> {
            val cards = mutableListOf<Flashcard>()
            for (i in 1..10) {
                cards.add(Flashcard("Term $i", "Def $i", null, 0))
            }
            return cards
        }
    }

     
}

@Dao
interface FlashcardDao{

    @Query("select * from flashcard where setId = :id")
    suspend fun getForSet(id:Long):Flashcard

    @Query("select * from flashcard")
    suspend fun getAll():List<Flashcard>

    @Insert
    suspend fun insertAll(fc: cs.mad.flashcards.entities.Flashcard)

    @Update
    suspend fun update(fc: Flashcard)

    @Delete
    suspend fun delete(fc: Flashcard)

    @Query("delete from flashcard")
    suspend fun deleteAll()
}