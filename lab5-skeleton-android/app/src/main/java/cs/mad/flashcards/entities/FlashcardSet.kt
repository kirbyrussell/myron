package cs.mad.flashcards.entities

import androidx.room.*


@Entity
data class FlashcardSet( var title: String, @PrimaryKey val id: Long? = null ) {
    companion object {
        fun getHardcodedFlashcardSets(): List<FlashcardSet> {
            val sets = mutableListOf<FlashcardSet>()
            for (i in 1..10) {
                sets.add(FlashcardSet(title = "Set $i"))
            }
            return sets
        }
    }
}


@Dao
interface FlashcardSetDao{

    @Query("select * from flashcardSet")
    suspend fun getAll():List<FlashcardSet>

    @Insert
    suspend fun insertAll(fcs: cs.mad.flashcards.entities.FlashcardSet):Long

    @Update
    suspend fun update(fcs: List<FlashcardSet>)

    @Delete
    suspend fun delete(fcs: FlashcardSet)

    @Query("delete from flashcardSet")
    suspend fun deleteAll()
}

