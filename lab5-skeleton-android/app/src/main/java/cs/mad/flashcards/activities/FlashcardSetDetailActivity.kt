package cs.mad.flashcards.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import cs.mad.flashcards.adapters.FlashcardAdapter

import cs.mad.flashcards.databinding.ActivityFlashcardSetDetailBinding

import cs.mad.flashcards.entities.FlashcardDao
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.entities.FlashcardSetDao
import cs.mad.flashcards.runOnIO


class FlashcardSetDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityFlashcardSetDetailBinding
    private lateinit var fcDao: FlashcardDao
    private lateinit var fcsDao: FlashcardSetDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFlashcardSetDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val db = Room.databaseBuilder(applicationContext, FlashcardDatabase::class.java, FlashcardDatabase.databaseName).build()
        fcDao = db.fcDao()
        fcsDao = db.fcsDao()


        runOnIO {


            binding.flashcardList.adapter = FlashcardAdapter(fcDao.getAll(), db)

        }




        binding.addFlashcardButton.setOnClickListener {
            var itemcount = (binding.flashcardList.adapter as FlashcardAdapter).itemCount + 1

            var fc = Flashcard("Term $itemcount", "Definition $itemcount", null, 1)





                (binding.flashcardList.adapter as FlashcardAdapter).addItem(fc)





            runOnIO {

                //val setId = fcsDao.insertAll(FlashcardSet("Set $itemcount", fc.setId))
                val id = fcDao.insertAll(fc)
                Log.d("Id: ",id.toString())
            }



            binding.flashcardList.smoothScrollToPosition((binding.flashcardList.adapter as FlashcardAdapter).itemCount - 1)
        }




        binding.studySetButton.setOnClickListener {
            startActivity(Intent(this, StudySetActivity::class.java))
        }

        binding.deleteSetButton.setOnClickListener {

            
            startActivity(Intent(this, MainActivity::class.java))



        }
    }
}