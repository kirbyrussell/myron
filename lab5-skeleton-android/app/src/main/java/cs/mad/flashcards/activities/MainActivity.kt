package cs.mad.flashcards.activities

import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.databinding.ActivityMainBinding
import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.entities.FlashcardSetDao
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import cs.mad.flashcards.runOnIO


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var prefs: SharedPreferences

    private lateinit var fcsDao: FlashcardSetDao



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val db = Room.databaseBuilder(applicationContext, FlashcardDatabase::class.java, FlashcardDatabase.databaseName).build()
        fcsDao = db.fcsDao()



        runOnIO {
            //fcsDao.deleteAll()

            binding.flashcardSetList.adapter = FlashcardSetAdapter(fcsDao.getAll())
        }



        binding.createSetButton.setOnClickListener {
            var itemcount = (binding.flashcardSetList.adapter as FlashcardSetAdapter).itemCount + 1

            var fcs = FlashcardSet("Set $itemcount")
            (binding.flashcardSetList.adapter as FlashcardSetAdapter).addItem(fcs)




                    runOnIO {


                       val id =  fcsDao.insertAll(fcs)
                        Log.d("Id: ",id.toString())
                    }



                binding.flashcardSetList.smoothScrollToPosition((binding.flashcardSetList.adapter as FlashcardSetAdapter).itemCount - 1)


            }





    }









}





