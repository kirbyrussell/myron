package cs.mad.flashcards.activities

import androidx.room.Database
import androidx.room.RoomDatabase
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardDao
import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.entities.FlashcardSetDao

@Database(entities = [Flashcard::class, FlashcardSet::class], version = 1)
abstract class FlashcardDatabase:RoomDatabase() {
    companion object{
        const val databaseName =  "FLASHCARD_DATABASE"
    }
    abstract fun fcDao(): FlashcardDao
    abstract fun fcsDao(): FlashcardSetDao
}