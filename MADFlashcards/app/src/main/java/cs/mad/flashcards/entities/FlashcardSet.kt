package cs.mad.flashcards.entities

data class FlashcardSet(var title: String)

fun cardSet(): MutableList<FlashcardSet> {
    val cardsList = mutableListOf<FlashcardSet>()

    for(i in 1..10){
        cardsList.add(FlashcardSet(i.toString()))
    }

    return cardsList
}
