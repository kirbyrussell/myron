package cs.mad.flashcards

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import cs.mad.flashcards.entities.cardSet
import cs.mad.flashcards.entities.cards

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.d("", cards().toString())
        Log.d("", cardSet().toString())
    }


}