package cs.mad.flashcards.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.entities.*






/*
===================================================================================================================

     Reference documentation for recyclers: https://developer.android.com/guide/topics/ui/layout/recyclerview

===================================================================================================================
 */

class MainActivity : AppCompatActivity(){

    private var recyclerView: RecyclerView? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var myList = FlashcardSet.getHardcodedFlashcardSets()



        recyclerView = findViewById(R.id.RecyclerView)
        recyclerView?.adapter = FlashcardSetAdapter()

        val addButt = findViewById<Button>(R.id.add)

        addButt.setOnClickListener({var ad = recyclerView?.adapter as FlashcardSetAdapter
                ad.add()
        Toast.makeText(this, "You've selected the add button", Toast.LENGTH_LONG).show()})





            /*
                connect to views using findViewById
                setup views here - recyclerview, button
                don't forget to notify the adapter if the data set is changed
             */

    }


}


