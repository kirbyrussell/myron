package cs.mad.flashcards.activities



import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Switch
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.entities.Flashcard

/*
===================================================================================================================

     Reference documentation for recyclers: https://developer.android.com/guide/topics/ui/layout/recyclerview

===================================================================================================================
 */

class FlashcardSetDetailActivity : AppCompatActivity(), View.OnClickListener {

    private var recyclerView2: RecyclerView? = null
    private var myList2 = Flashcard.getHardcodedFlashcards().toMutableList()

    override fun onCreate(savedInstanceState: Bundle?)  {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_set_detail)


        recyclerView2 = findViewById(R.id.RecyclerView_2)
        recyclerView2?.adapter = FlashcardAdapter()


        val addButt = findViewById<Button>(R.id.button_add)
        val studyButt = findViewById<Button>(R.id.button_study)
        val delButt = findViewById<Button>(R.id.button_delete)

        addButt.setOnClickListener(this)
        studyButt.setOnClickListener (this)
        delButt.setOnClickListener(this)

    }

    override fun onClick(View : View){


        if (View.id == R.id.button_add) {
            var ad = recyclerView2?.adapter as FlashcardAdapter
            ad.add()
            Toast.makeText(this, "You've selected the add button", Toast.LENGTH_LONG).show()
        }
        if (View.id == R.id.button_study) {
            Toast.makeText(this, "You've selected the study button", Toast.LENGTH_LONG).show()
        }
        if (View.id == R.id.button_delete) {

            var ad = recyclerView2?.adapter as FlashcardAdapter
            ad.del()
            Toast.makeText(this, "You've selected the delete button", Toast.LENGTH_LONG).show()
        }

        





    }
}


