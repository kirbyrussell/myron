package cs.mad.flashcards.activities

import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.get
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.databinding.ActivityFlashcardSetDetailBinding
import cs.mad.flashcards.databinding.CustomDialogueBinding
import cs.mad.flashcards.databinding.StudysetBinding
import cs.mad.flashcards.entities.Flashcard
import java.net.URL


class StudySetActivity : AppCompatActivity() {
    private lateinit var binding: StudysetBinding
    private lateinit var binding2: ActivityFlashcardSetDetailBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = StudysetBinding.inflate(layoutInflater)
        binding2 = ActivityFlashcardSetDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.flashcardSetTitle.setText(Flashcard.getHardcodedFlashcards()[0].question)
        binding.flashcardSetTitle.setOnClickListener {


            Toast.makeText(this, Flashcard.getHardcodedFlashcards()[0].answer, Toast.LENGTH_LONG).show()


        }

        //val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com"))
        binding.exitButton.setOnClickListener {
            binding.exitButton.context.startActivity(Intent(binding.exitButton.context, FlashcardSetDetailActivity::class.java))
            //binding.exitButton.context.startActivity(intent)

        }

        /*
        val view = binding.root
        setContentView(view)

        binding.flashcardList.adapter = FlashcardAdapter(Flashcard.getHardcodedFlashcards())

        binding.addFlashcardButton.setOnClickListener {
            (binding.flashcardList?.adapter as FlashcardAdapter).addItem(Flashcard("test", "test"))
            binding.flashcardList.smoothScrollToPosition((binding.flashcardList.adapter as FlashcardAdapter).itemCount - 1)
        }

         */

    }


}