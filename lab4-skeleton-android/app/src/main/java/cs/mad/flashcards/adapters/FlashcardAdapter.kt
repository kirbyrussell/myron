package cs.mad.flashcards.adapters

import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.activities.FlashcardSetDetailActivity
import cs.mad.flashcards.databinding.CustomDialogueBinding
import cs.mad.flashcards.entities.Flashcard

class FlashcardAdapter(dataSet: List<Flashcard>) :
    RecyclerView.Adapter<FlashcardAdapter.ViewHolder>() {

    private var dataSet = dataSet.toMutableList()


    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val flashcardTitle: TextView = view.findViewById(R.id.flashcard_title)








    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_flashcard, viewGroup, false)
        )
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val item = dataSet[position]
        viewHolder.flashcardTitle.text = item.question




        fun alertFun2(): Boolean {
            val dialogueBinding =
                CustomDialogueBinding.inflate(LayoutInflater.from(viewHolder.itemView.context))




            val alert2 = AlertDialog.Builder(viewHolder.itemView.context)


            val db1 = dialogueBinding.eText1
            val db2 = dialogueBinding.eText2


            alert2.setTitle(item.question)
            alert2.setMessage(item.answer)
            alert2.setView(dialogueBinding.root)
                .setPositiveButton("Done") { dialogInterface: DialogInterface, i: Int ->
                    // do something on click


                    item.question = db1.text.toString()
                    item.answer = db2.text.toString()
                    notifyDataSetChanged()


                }


                .setNegativeButton("Delete") { dialogInterface: DialogInterface, i: Int ->

                dialogInterface.dismiss()
                }

                .create()
                .show()


        return true
        }

        fun alertFun(){

            val alert1 = AlertDialog.Builder(viewHolder.itemView.context)
            alert1.setTitle(viewHolder.flashcardTitle.text)
            alert1.setMessage(item.answer)

            alert1.setPositiveButton("Edit") { dialogInterface: DialogInterface, i: Int ->
                // do something on click
                alertFun2()



            }
                .create()
                .show()
        }



        viewHolder.itemView.setOnClickListener {


            alertFun()

        }

        viewHolder.itemView.setOnLongClickListener {
            alertFun2()
        }


    }








    override fun getItemCount(): Int {
        return dataSet.size
    }

    fun addItem(it: Flashcard) {
        dataSet.add(it)
        notifyItemInserted(dataSet.lastIndex)
    }









}


